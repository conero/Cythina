<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/member/quit', 'Member@loginOut');
Route::get('/member', 'Member@Index');
Route::get('/user', 'User@index');

Route::get('/finance/bill', 'finance\\Bill@index');
Route::get('/finance', 'Finance@index');

Route::post('/auth/login', 'Auth@login');
Route::get('/', 'Home@Index');




//Route::get('/', function () {
//    return view('welcome');
//});
