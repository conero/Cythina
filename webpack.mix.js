const mix = require('laravel-mix');

let version = require('./resources/node/version')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// 版本自动生成脚本
new version()

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');

// mix.react('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

// 首页 - 2017年11月18日 星期六
mix.react('resources/js/home/main.js', 'public/js/home/')
//    .sass('resources/assets/css/home.css', 'public/css')
//    .sass('resources/assets/sass/home.scss', 'public/css')
;   
mix.react('resources/js/member/index.js', 'public/js/member/');
mix.react('resources/js/user/index.js', 'public/js/user/')

mix.react('resources/js/finance/index.js', 'public/js/finance/')
mix.react('resources/js/finance/bill.js', 'public/js/finance/')