/**
 * 2018年5月8日 星期二
 * 版本生成信息
**/
const Pkg = require('../../package.json')
const fs = require('fs')

module.exports = class Version{
    constructor(){
        this.createPhpConfig()
        this.createJsConfig()
    }
    /**
     * 生词php配置脚本
     */
    createPhpConfig(){
        /*
            return [
                'version' => '0.0.2',
                'build'   => '20171207',
                'since'   => '20171118',
                'copyright' => '@conero.cn',
                'author'    => 'Joshua Conero(JC)',
                'author_zh' => '古丞秋'
            ];
        */
        let phpspt = 
`<?php
// ${new Date()}
// create by nodeJs
return [
    'version'   => '${Pkg.version}',
    'build'     => '${Pkg.release}',
    'since'     => '${Pkg.since}',
    'copyright' => '${Pkg.copyright || '@conero.cn'}',
    'author'    => '${Pkg.author.name}',
    'author_zh' => '${Pkg.author.zhName}',
    'code'      => '${Pkg.name}',
]
;
        `
        fs.writeFileSync('./config/version.php', phpspt)
    }
    /**
     * js 文档生成
     */
    createJsConfig(){
        let jsScp = 
`// ${new Date()}
// create by nodeJs
export default {
    version:    '${Pkg.version}',
    build:      '${Pkg.release}',
    since:      '${Pkg.since}',
    copyright:  '${Pkg.copyright || '@conero.cn'}',
    author:     '${Pkg.author.name}',
    author_zh:  '${Pkg.author.zhName}',
    code:       '${Pkg.name}'
}
;
`
        fs.writeFileSync('./resources/js/version.js', jsScp)
    }
}