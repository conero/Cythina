
/**
 * 项目基本脚本
 */
// 配置信息
import Config from './config'
import Version from './version'

class Cythina{
    constructor(){
        // console.log(Config)
    }
    /**
     * 获取服务端放回的数据
     * @param {string} key 
     * @param {any} def 
     */
    args(key, def){
        if(__args && 'object' == typeof __args){
            return __args[key] || def
        }
        return def
    }
    /**
     * @param {string} selector 
     */
    query(selector){
        return document.querySelector(selector)
    }
    /**
     * 获取版本信息
     * @param {string} key
     * @returns {*}
     */
    static version(key){
        if(key){
            return Version[key] || null
        }
        return Version
    }
    /**
     * 获取配置信息
     */
    /**
     * @static
     * @param {string} key
     * @returns
     * @memberof Cythina
     */
    static config(key){
        if(key){
            return Config[key] || null
        }
        return Config
    }
    /**
     * 获取 pigeon url 
     * @param {string} url 
     */
    static api(url){
        return Config.api.baseurl + url
    }
    /**
     * token 操作
     * @param {string} token 
     */
    static token(token){
        const key = 'cythina_token'
        // 删除 token 参数
        if(false === token){
            sessionStorage.removeItem(key)
            return true;
        }else if(token){
            sessionStorage.setItem(key, token)
            return true;
        }else{
            return sessionStorage.getItem(key);
        }
    }
}
export default Cythina