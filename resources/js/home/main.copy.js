/**
 * 2017年11月18日 星期六
 * 首页应用
 */

/* eslint no-nested-ternary:0 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { 
  Card, WingBlank, WhiteSpace , Flex, 
  Menu, ActivityIndicator, NavBar, Modal
} 
  from 'antd-mobile';

import Cythina from '../cythina'  
const Cy = new Cythina()


const data = [
  {
    value: '1',
    label: 'Food',
    children: [
      {
        label: 'All Foods',
        value: '1',
        disabled: false,
      },
      {
        label: 'Chinese Food',
        value: '2',
      }, {
        label: 'Hot Pot',
        value: '3',
      }, {
        label: 'Buffet',
        value: '4',
      }, {
        label: 'Fast Food',
        value: '5',
      }, {
        label: 'Snack',
        value: '6',
      }, {
        label: 'Bread',
        value: '7',
      }, {
        label: 'Fruit',
        value: '8',
      }, {
        label: 'Noodle',
        value: '9',
      }, {
        label: 'Leisure Food',
        value: '10',
      }],
  }, {
    value: '2',
    label: 'Supermarket',
    children: [
      {
        label: 'All Supermarkets',
        value: '1',
      }, {
        label: 'Supermarket',
        value: '2',
        disabled: true,
      }, {
        label: 'C-Store',
        value: '3',
      }, {
        label: 'Personal Care',
        value: '4',
      }],
  },
  {
    value: '3',
    label: 'Extra',
    isLeaf: true,
    children: [
      {
        label: 'you can not see',
        value: '1',
      },
    ],
  },
];

class MenuExample extends React.Component {
  constructor(...args) {
    super(...args);
    this.state = {
      initData: '',
      show: false,
    };
  }
  onChange = (value) => {
    let label = '';
    data.forEach((dataItem) => {
      if (dataItem.value === value[0]) {
        label = dataItem.label;
        if (dataItem.children && value[1]) {
          dataItem.children.forEach((cItem) => {
            if (cItem.value === value[1]) {
              label += ` ${cItem.label}`;
            }
          });
        }
      }
    });
    console.log(label);
  }
  handleClick = (e) => {
    e.preventDefault(); // Fix event propagation on Android
    this.setState({
      show: !this.state.show,
    });
    // mock for async data loading
    if (!this.state.initData) {
      setTimeout(() => {
        this.setState({
          initData: data,
        });
      }, 500);
    }
  }

  onMaskClick = () => {
    this.setState({
      show: false,
    });
  }

  render() {
    const { initData, show } = this.state;
    const menuEl = (
      <Menu
        className="foo-menu"
        data={initData}
        value={['1', '3']}
        onChange={this.onChange}
        height={document.documentElement.clientHeight * 0.6}
      />
    );
    const loadingEl = (
      <div style={{ width: '100%', height: document.documentElement.clientHeight * 0.6, display: 'flex', justifyContent: 'center' }}>
        <ActivityIndicator size="large" />
      </div>
    );
    return (
      <div className={show ? 'menu-active' : ''}>
        <div>
          <NavBar
            leftContent="Cythina"
            mode="light"
            icon={<img src="https://gw.alipayobjects.com/zos/rmsportal/iXVHARNNlmdCGnwWxQPH.svg" className="am-icon am-icon-md" alt="" />}
            onLeftClick={this.handleClick}
            className="top-nav-bar"
          >
            Coming up
          </NavBar>
        </div>
        {show ? initData ? menuEl : loadingEl : null}
        {show ? <div className="menu-mask" onClick={this.onMaskClick} /> : null}
      </div>
    );
  }
}

ReactDOM.render(<MenuExample />, document.getElementById('container'));

window.$ = window.jQuery = require('jquery');
// 渲染 markdown
var showdown  = require('showdown'),
// var
converter = new showdown.Converter(),
text      = 
    '<h2> Cythina-0.0.1/20171118</h2>'
    + '- 服务器端搭建'
    + ' - 基于对 nginx/Apache 等服务器在云上的二级域名的实现，决定在原来的weui的基础上。重构应用； 两者将代表传统的web构建方式以及现代web构建方式的对比'
    + '    - 本地搭建 nginx 实验性项目'
    + '   - 项目采用最新的php技术 php7.1.x'
    + '        - 利用 composer 搭建基于 laravel 5.5.21 的项目 Cythina'
    + '        - $ composer create-project --prefer-dist laravel/laravel Cythina'
    + '        - $ composer install'
    + '    - 创建主键面 /Home 控制器'
    + '       - 公共头部设置'
    + '- 前端搭建'
    + '   - 利用*** $ php artisan preset react*** 搭建 laravel 的 react基本脚手架'
    + '    - 下载 ant-design 2.0.3'
    + '       - 使用 npm 管理包 ***npm install***'
    + '   - 搭建调试， 根据应用的相关错误安装额外的创建'
    + '- 基本上实现应用的入门 '
,
html = converter.makeHtml(text)
;
console.log(converter.makeHtml('*Hello* __World__'))
console.log(html);
console.log(showdown);

var testmodel = false
ReactDOM.render(
  <WingBlank size="lg">    
    <WhiteSpace size="lg" />
    <Card>
      <Card.Header
          // title="Susanna"
          thumb="/image/Susanna Roong.Conero.jpg"
          thumbStyle={{'width': '80%', 'height':'80%'}}
          // extra={<span>version 0.0.1</span>}
        />
      <Card.Body>
        <div>
          I'love you more than you think. baby! 
          <br/> Susanna And I making a better life.
          <br/> Love me, but don't leave me. 
          <br/> Cuase I don't wanna be sad anymore, goddess.
        </div>
      </Card.Body>
      <Card.Footer content="2017-12-08" extra={<div>Joshua Conero</div>} />
    </Card>
    <WhiteSpace size="lg" />
    <Card>
      <Card.Header
        title="beging"
        thumb="/cythina.png"
        extra={<span>version 0.0.1</span>}
      />
      <Card.Body>
        {/* <div>{html}</div> cross-site scripting (XSS) */}
        <div dangerouslySetInnerHTML={{__html: html}} />
      </Card.Body>
      <Card.Footer content="2017-11-18" extra={<div>Joshua Conero</div>} />
    </Card>
    <WhiteSpace size="lg" />
    {/* Modal 窗 */}
    <Modal
      visible={testmodel}
      transparent
      maskClosable={true}
      // onClose={this.onClose('modal1')}
      title="Susanna"
      footer={[{ text: ':)-', onPress: () => {testmodel = false; } }]}
      wrapProps={{ onTouchStart: this.onWrapTouchStart }}
    >
      {/* <div style={{ height: 100, overflow: 'scroll' }}> */}
      <div>
      I'love you more than you think. baby! 
      <br/> Susanna And I making a better life.
      <br/> Love me, but don't leave me. 
      <br/> Cuase I don't wanna be sad again anymore, goddess.
      </div>
    </Modal>
  </WingBlank>
  ,Cy.query('#top_card')
)

const Version = () => (
  <div>VERSION {Cy.args('version')} ({Cy.args('build')})</div>
)

ReactDOM.render(
  <div>
    <Flex justify="center"><Version /></Flex>
    {/* <Flex justify="center">since {Cy.args('since')}</Flex> */}
    <Flex justify="center">copyright {Cy.args('copyright')}</Flex>
    <Flex justify="center">author {Cy.args('author')}</Flex>
    {/* <Flex justify="center">author-zh {Cy.args('author_zh')}</Flex> */}
    {/* <Flex justify="center">date {Cy.args('date')}</Flex>     */}
  </div>
  ,Cy.query('#footer')
)

