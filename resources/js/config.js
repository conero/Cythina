/**
 * 配置系统处理
 * 2017年12月8日 星期五
 */
const env = process.env.NODE_ENV
// 配置信息
var Config = {}
// 发布模式
if('development' == env){
    Config.api = {
        baseurl : 'http://127.0.0.1:7002/'
    }
}else{
    Config.api = {
        baseurl : 'https://api.conero.cn/'
    }
}

export default Config