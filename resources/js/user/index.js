/**
 * 2018年9月9日 星期日
 * 用户中心
 */
import ReactDOM from 'react-dom'
import React from 'react'

import {
    // BrowserRouter as Router, 
    HashRouter as Router, 
    Route, Link
} from 'react-router-dom'

import {
    NavBar, Icon, Grid, WhiteSpace, Card 
} from 'antd-mobile'

import Cythina from '../cythina'
import VersionFooter from '../components/VersionFooter'


var Cy = new Cythina()
const Account = Cy.args('account'),
    Name = Cy.args('name')


class UserCenter extends React.Component{
    gridClickHld(el, index){
        if(el.link){
            location.href = el.link
        }
    }
    render(){
        let gridData = [
            {icon: '', text: '财务中心', link: '/finance'}
        ]
        return (
        <div>
            <NavBar
                mode='light'
                icon={<Icon type="left" />}
                onLeftClick={() => {
                    location.href = '/'
                }}
            >
                用户中心
            </NavBar>
            <WhiteSpace />
            
            {/* 欢迎语 */}
            <Card>
                <Card.Header 
                    title={Account}
                    extra={Name}
                />
                <Card.Body><div>您已经成功登录系统！</div></Card.Body>
            </Card>
            
            <WhiteSpace />
            <div style={{minHeight: '520px'}}>
                <Grid
                onClick={this.gridClickHld}
                data={gridData}></Grid>
            </div>
            <WhiteSpace />
            <VersionFooter />
        </div>
        )
    }
}

ReactDOM.render(
    <Router>
        <div>
            {/* <Link to="/login">登录系统</Link> */}
            {/* <Link to="/register">注册</Link> */}
            <Route path="/" component={UserCenter} />
        </div>
    </Router>
    , Cy.query('#container')
)