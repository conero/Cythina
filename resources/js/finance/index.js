/**
 * 2018年9月9日 星期日
 * 财务中心
 */
import ReactDOM from 'react-dom'
import React from 'react'

import {
    // BrowserRouter as Router, 
    HashRouter as Router, 
    Route, Link
} from 'react-router-dom'

import {
    NavBar, Icon, Grid, WhiteSpace, NoticeBar
} from 'antd-mobile'

import Cythina from '../cythina'
import VersionFooter from '../components/VersionFooter';


var Cy = new Cythina()


class UserCenter extends React.Component{
    gridClickHld(el, index){
        if(el.link){
            location.href = el.link
        }
    }
    render(){
        let gridData = [
            {icon: '', text: '财务账单', link: '/finance/bill'}
        ]
        return (
        <div>
            <NavBar
                mode='light'
                icon={<Icon type="left" />}
                onLeftClick={() => {
                    location.href = '/user'
                }}
            >
                财务中心
            </NavBar>

            <WhiteSpace />
            
            <NoticeBar 
                mode="link"
                onClick={() => location.href='/'}
                >
                返回首页
            </NoticeBar>

            <WhiteSpace />
            <div style={{minHeight: '520px'}}>
                <Grid
                onClick={this.gridClickHld}
                data={gridData}></Grid>
                <WhiteSpace />
            </div>
            <VersionFooter />
        </div>
        )
    }
}

ReactDOM.render(
    <Router>
        <div>
            {/* <Link to="/login">登录系统</Link> */}
            {/* <Link to="/register">注册</Link> */}
            <Route path="/" component={UserCenter} />
        </div>
    </Router>
    , Cy.query('#container')
)