/**
 * 2018年9月9日 星期日
 * 财务中心
 */
import ReactDOM from 'react-dom'
import React from 'react'

import {
    // BrowserRouter as Router, 
    HashRouter as Router, 
    Route
} from 'react-router-dom'

import {
    NavBar, Icon, List, WhiteSpace, ListView,
    Toast
} from 'antd-mobile'


import Cythina from '../cythina'
import {Pigeon, Variable} from 'nestjs'
import Axios from 'axios';
import VersionFooter from '../components/VersionFooter';




// 用于测试 nestjs 库
// test - pigeon
// (() => {
//     let pg = new Pigeon({})
//     console.log(pg.url('finance/account/{user}/sets', {user: 'test'}))
// })();

var Cy = new Cythina()
const Pg = new Pigeon({})
const User = Cy.args('user')

class UserCenter extends React.Component{
    constructor(props) {
        super(props);        

        // const getSectionData = (dataBlob, sectionID) => dataBlob[sectionID];
        const getSectionData = (dataBlob, sectionID) => {
            // console.log(dataBlob);
            
            return dataBlob[sectionID]
        }
        // const getRowData = (dataBlob, sectionID, rowID) => dataBlob[rowID];
        const getRowData = (dataBlob, sectionID, rowID) => {
            // console.log(dataBlob[sectionID][rowID]);
            
            return dataBlob[sectionID][rowID]
        }

        const dataSource = new ListView.DataSource({
            getRowData,
            getSectionHeaderData: getSectionData,
            rowHasChanged: (row1, row2) => row1 !== row2,
            sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
        })

        this.state.dataSource = dataSource
    }
    state = {
        dataSource: null,
        isLoading: true,
        height: document.documentElement.clientHeight * 3 / 4,
        hasMore: true
    }
    gridClickHld(el, index){
        if(el.link){
            location.href = el.link
        }
    }
    componentDidMount(){
        
        // load new data
        // hasMore: from backend data, indicates whether it is the last page, here is false
        if (this.state.isLoading && !this.state.hasMore) {
            return;
        }
        let url = Pg.url('finance/account/{user}/sets', {user: User})
        url = Cythina.api(url)
        
        let headers = {}
        headers[Variable.getHeaderKey('token')] = Cythina.token()
        headers[Variable.getHeaderKey('code')] = Cythina.version('code')
        
        // console.log(headers)
        Axios.get(url, {headers}).then((rs) => {
            // console.log(rs)
            let dd = rs.data
            if(dd.code == 400){
                Toast.fail(dd.msg)
            }else{
                this.setState({
                    //dataSource: this.state.dataSource,
                    dataSource: this.state.dataSource.cloneWithRows(dd.data),
                    isLoading: false
                })
            }            
        })
    }
    render(){
        const row = (rowData, sectionID, rowID) => {
            // console.log(rowData, sectionID, rowID);
            return (
                <List.Item 
                    actions={[<a>编辑</a>, <a>删除</a>]}
                    key={rowID} 
                    extra={rowData.date}>
                    ￥ {rowData.money}
                </List.Item>     
            )
        }

        return (
        <div>
            <NavBar
                mode='light'
                icon={<Icon type="left" />}
                onLeftClick={() => {
                    location.href = '/finance'
                }}
            >
                财务账单
            </NavBar>

            <WhiteSpace /> 
            <List renderHeader={() => '财务账单'} itemLayout='horizontal'>
                <ListView 
                    ref={el => this.lv = el}
                    dataSource={this.state.dataSource}
                    renderHeader={() => <span>header</span>}
                    renderFooter={() => (<div style={{ padding: 30, textAlign: 'center' }}>
                        {this.state.isLoading ? 'Loading...' : 'Loaded'}
                        </div>)}
                    style={{
                        height: this.state.height,
                        overflow: 'auto',
                    }}
                    renderRow={row}
                />
            </List>

            <VersionFooter />
        </div>
        )
    }
}

ReactDOM.render(
    <Router>
        <div>
            {/* <Link to="/login">登录系统</Link> */}
            {/* <Link to="/register">注册</Link> */}
            <Route path="/" component={UserCenter} />
        </div>
    </Router>
    , Cy.query('#container')
)