// Mon Oct 29 2018 23:41:45 GMT+0800 (中国标准时间)
// create by nodeJs
export default {
    version:    '0.1.7-alpha.1',
    build:      '20181029',
    since:      '20171118',
    copyright:  '@conero.cn',
    author:     'Joshua Conero(JC)',
    author_zh:  '古丞秋',
    code:       'cythina'
}
;
