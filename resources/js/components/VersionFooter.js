/**
 * 2018年9月12日 星期三
 * 版本描述页面底页
 */

import React from 'react';
import {Flex} from 'antd-mobile'

import Cythina from '../cythina'
const Version = Cythina.version()


export default class VersionFooter extends React.Component{
    state = {
        // 自动匹配高度
        autoSize: false,
        style: {
            marginTop: 2
        }        
    }
    constructor(props){        
        super(props)
        if(props && props.autoSize){
            this.state.autoSize = props.autoSize
        }
        if(props.style){
            this.state = props.style
        }
        this.cyFooter = React.createRef()
    }
    /**
     * @todo  实现部件自动适应到页面底部
     * @memberof VersionFooter
     */
    _autoSizeHandle(){
        // cyhina-footer
        let $cyf = this.cyFooter.current
        // 屏幕高度
        let h = document.documentElement.offsetHeight || document.body.offsetHeight;
        let detH = h - $cyf.offsetTop
        if(detH > 10){
            // $cyf.style.MarginTop = detH
            let style = Object.assign({}, this.state.style)
            if(!style){
                style = {}
            }
            style.marginTop = detH
            
            this.setState({
                style
            })
        }

        // console.log($cyf, $cyf.offsetTop, h, detH);
    }
    componentDidMount(){
        if(this.state.autoSize){
            this._autoSizeHandle()
        }   
    }    
    render(){
        return (
            // <div style={{marginTop}}></div>
            <div style={this.state.style} className="cyhina-footer" ref={this.cyFooter}>
                <Flex justify="center"><div>VERSION {Version.version} ({Version.build})</div></Flex>
                {/* <Flex justify="center">since {Cy.args('since')}</Flex> */}
                <Flex justify="center">copyright {Version.copyright}</Flex>
                <Flex justify="center">author {Version.author}</Flex>
                {/* <Flex justify="center">author-zh {Cy.args('author_zh')}</Flex> */}
                {/* <Flex justify="center">date {Cy.args('date')}</Flex>     */}
            </div>
        )
    }
}