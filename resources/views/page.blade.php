@include('cythina')
<body>
<div id="container"></div>

@if (isset($_page['js']))
    <script src="{{ $_page['js'] }}"></script>
@endif

</body>
</html>