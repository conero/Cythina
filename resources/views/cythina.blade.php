{{-- 公共头部 --}}
{{-- 2017年11月18日 星期六 --}}
        <!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="keywords" content="服务,管理,conero,doeeking">
    <meta name="description" content="个人服务以及管理系统移动应用">
    <meta name="author" content="Joshua Conero,brximl@163.com">
    <meta name="copyright" content="Conero.cn">
    {{-- 开发模式下，前端js不再缓存 --}}
    @if ('local' == env('APP_ENV'))
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="0">
        <meta http-equiv="Cache" content="no-cache">
    @endif

    <link rel="shortcut icon" href="{{asset('/cythina.ico')}}"/>
    <script>
        var __CENTER = {
            'csrf': '{{csrf_token()}}'
        };
    </script>
    {{-- title --}}
    @if (isset($a__tpl['title']))
        <title>{{ $a__tpl['title'] }}</title>
    @else
        <title>Cythina</title>
    @endif

    {{-- css 样式 --}}
    @if (isset($a__tpl['css']))
        {!! $a__tpl['css'] !!}
    @endif

    {{-- 前端脚本传递 --}}
    @if (isset($a__script))
        <script> var __args = {!! $a__script !!};</script>
    @endif
</head>