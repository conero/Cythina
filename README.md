# Cythina-mobile.aurora-ant
- 2017年11月18日 星期六
- ant-reactJs

## 关于

> 项目更新日志 [VERSION.md](./VERSION.md)

> Todo List [doc/todo.md](./doc/todo.md)



## laravel 5.7.1
- 服务器：  nginx
- composer create-project --prefer-dist laravel/laravel Cythina 创建项目
    - php artisan key:generate  生成项目key值
    - php artisan preset react 
        - [Laravel 5.5 新增 React、Bootstrap 等前端脚手架 ](https://9iphp.com/web/laravel/laravel-frontend-presets.html)
- [mobile.ant.design](https://mobile.ant.design/index-cn)



## ant-design 

*在原来的 weui 基础上，采用 ant-design UI重新构建*

>
在 VueJs + element-ui ([aurora/center](http://www.conero/center)) 基础上，扩展新的知识领域
ReactJs + ant-design 构建

服务器端采用 php7.1.xx， 后期将持续更新

将 weui 的应用迁移到 Cythina 项目，后者与***aurora/center***一直，数据通过相同的接口获取。因此应用将最先实现数据渲染

> antd-mobile 安装与更新
> - npm install antd-mobile --save    安装
> - npm update antd-mobile            更新



## 依赖包
### conero
> *pigeons/nest-php*
```json
{
  "psr-4": {
    "pigeons\\nest\\":"vendor/conero/src/"
  }
}
```