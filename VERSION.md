# 版本更新
## Cythina v 0.1

- 环境
    - laravel @5.7(5.7.1)
    - yarn

### v0.1.7/20181029 (alpha)

#### alpha.1/181029

- **页面** 
  - */finance/bill*
    - (修复) *修复 ListView 渲染无效*
    - (优化) *初步设计财务账单*
    - (+) *引入 VersionFooter*

### v0.1.6/20181028

> 编写 todo-list 关于的 *财务账单* 的设计预想

- **前端库**
  - (优化) *components/VersionFooter*
    - 完善*props* 使 *autoSize* 奏效，但默认为 `false`
    - (+) 添加方法 *_autoSizeHandle* 实现对底部宽度的自适应，搭建处理环境





### v0.1.5/20181027

- **前端库**
  - (优化) *components/VersionFooter*
    - 函数组件类化，支持传入 `props.style` 设置版本发的样式
- **页面**
  - */member#/login*
    - (优化) *登录页面时添加提交后立刻是提交失效，防止重复提交*    `#181026`
    - (优化) *页面中使用组件~VersionFooter* 
- 依赖

  - *antd-mobile*   @2.2.4 -> 2.2.6    `#181026`




### v0.1.4/20181021

- **Cythina 库** 
  - (+) 类*Cyth* 新增方法 *getUserInfo* 用于获取当前登录用户的信息
  - (+) 类*Render* 添加方法 *title* 用于设置页面的标题
- **Http**
  - (优化) 类 *Controller* 方法优化对 *title* 配置的支持 `后期发现无效`
- **页面**
  - *User/ 用户控制类*      (*/user*)
    - (实现) *index* 类方法入当前的登录用户信息到前端
    - (优化/前端) *添加登录成功以后的欢迎语*
  - *Bill*/ 财务账单     	   (*/finance/bil*)
    - (实现) *index* 方法初步实现，将用户信息送至前端
    - (实现/前端) 引入 *ListView/长列表*  ，用于展示账单数据
  - *finance/ 财务中心*
    - (优化/前端) *添加转至首页链接*
  - */member 人员登录*
    - (优化/前端) *#/login* 登录成功以后计入token到当前的 *sessionStorage*
- **前端库**
  - *cythina.js*
    - (优化) *`static version`* 方法支持参数，用于获取指定的值
    - (优化) *`static config`* 方法可通过参数值获取对应的参数
    - (+) *static token* 实现保存/或者 sessionStorage 中的token
  - *node/version.js*
    - (+) *添加属性参数`code` 值*



### v0.1.3/20180916

> 1. 代码优化版本，完善等
>
> 2. 新增用户登录注销页面

- **http**
  - http: Member
    - (删除) 删除 *home.blade.php* 视图模板文件，使用 *page.blade.php* 
    - (+) 新增页面 */member/quit* 用于注销用户登录
  - http: Home
    - (优化) *用户登录成功以后添加“注销登录”的入口



### v0.1.2/20180915

> node 根据 *package.json* 生成 *version.js* 动态脚本，同时保留 *version.php* 脚本

- **http**
  - *http: Home*
    - (删除) 删除 *home.blade.php* 视图模板文件，使用 *page.blade.php* 
    - (调整) *home/main.js* 页面调整，使之符合目前*page.blade.php* 规范
  - (+) *finance,bill,user* 等添加*VersionFooter*
- **项目**
  - (优化) *Controller* 的*page* 方法优化js/data，支持后者



#### alpha.1/180912

> node 根据 *package.json* 生成 *version.js* 动态脚本，同时保留 *version.php* 脚本

- **项目**
  - (+) *App\Http\Controllers\Controller* 添加方法 *page* 实现所有页面使用同一的模板
  - (调整) *http： Finance/User/Bill* 删除对应独立的view视图，而采用统一的模板
- **assets**
  - (+) 添加*components\VersionFooter*  组件
- **webpack**
  - (添加) *resources/node/version.js* 添加 *js/version* 动态脚本的生成
- **依赖**
  - react/react-dom  *16.4.2->16.5.0*

### v0.1.1/20180909

> 添加*用户登录成功以后基本界面* 

- **页面**
  - (+) `/finance` 添加 *Finance* 控制器，搭建页面。新增前端脚本，以及php视图
    - 添加 *导航条* 以及九宫格，添加链接转至*/finance/bill(财务账单中)*
  - (+) `/finance/bill` 添加财务账单控制器，搭建基本的页面。新增前端脚本，以及php视图
  - (+) `/user` 添加 *User* 控制器，搭建的页面。新增前端脚本，以及php视图
    - 添加 *导航条* 以及九宫格，添加链接转至*/finance(财务中心)*
  - (优化) `/` 首页登录用户的条件下，添加跳转至用户中心的连接


### v-0.1.0/20180906

- 环境
  - (+) laravel 框架更新，根据官网文档  #5.7.1
  - (+) 引入个人处理包 *pigeon/nest-php* 用于实现与 *pigeon*的接口请求
  - (迁移) 将*0.0.x*中前端的版本迁移至新的环境中
- 系统
  - (+) *app/Cythina/Cyth* 类 添加方法 *envConf* 用于获取文件环境变量不同的配置
  - (实现) *app/Http/Controllers/Auth* 用于登录，实现*后台用户登录*

#### alpha.2/180906

- 项目
  - (+) *App\Cythina\Cyth* 类新增，实现项目固定的json错误/成功相应格式
- 页面
  - (+) `post:/auth/login` 新增 *Auth* 控制器，用于post格式的用户登录验证，搭建页面
  - (优化) `get:/member#/login` 用户登录与*Auth*请求连接
- 环境
  - 迁移以前的代码，*node* 脚本自动生成版本信息
- 依赖
  - antd-mobile -> 2.2.4





## Cythina v 0.0

> Cythina 初始版本

- 环境
    - laravel @5.5(5.5.23)

### Cythina-0.0.6@20180905
- 依赖
    - 2.1.8 -> antd-mobile@2.2.1
    + react-router-dom@4.3.1
    + babel-plugin-import@1.8.0
    + react-dom@16.4.2
    + lodash@4.17.10
    + showdown@1.8.6
    + laravel-mix@2.1.14
    + rc-form@2.2.2
    + react@16.4.2
    + jquery@3.3.1
    + antd-mobile@2.2.3

### Cythina-0.0.5@20180513
- 模块(公共页面)
    - /member Member 用户会员
        - LoginForm 用户登录页面吗，表单添加验证并做弹出提示
        - 页面引入 axios

### Cythina-0.0.4@20180509
- 模块(公共页面)
    - /member Member 用户会员
        - 使用 **react-router-dom** 实现页面内 “login/register” 模块之间的跳转；两者之间实现页面跳转
        - (+) 设计 **用户注册** 模块，按照需要的数据处理，利用 **rc-form** 实现表单数据生成
        - (优化) 利用 **用户注册** 模块的先关机技术，优化表单
    - / Home
        - 新增到 **登录/注册** 页面的跳转
- 依赖
    - (+) react-router-dom@4.2.2
    - (+) rc-form@2.2.0

### Cythina-0.0.3@20180508
- 包更新
    - axios@0.16.2->0.17.1                               @171209
    - axios@0.17.1->0.18.0                               @180508
    - antd-mobile@2.1.1->2.1.8
    - react/react-dom@16.1.1->16.3.2
- 系统
    - assets/js/cythina.js 包文案更新： 新增 dom selector 方法 @171209
    - views/cythina.blade.php: 公共头部浏览器缓存 meta 更新    @171209
    - assets/js/config.js 新增环境配置文件                    @171208
    - (+) 新增 assets/node/version.js 实现编译js时，自动生成php配置脚本信息，是版本信息由 **package.json** 管理
- 模块
    - /  Home 系统主页
        - js脚本重命名： js/home -> js/home/main.js 实现单页面应用      @171208   
        - 主页样式更新， 添加 **Susanna** 描述文本
        - 添加到登录页面的跳转实现页面入口

    - (+) /member  Member 用户会员
        - 新增控制器 **Http:Member** , 用时实现 “用户登录” 和 “用户注册”
        - 添加 js 脚本， member/index.js 初步实现“用户登录”界面；系统可以跳转至首页        
    

### Cythina-0.0.2@20171207
- 服务器端更新
    - composer update
        - laravel 5.5.21->5.5.23
- 前端更新
    - ant-design 包更新@2.0.3 ->2.1.1
- 系统
    - (新增) app/Cythina 下项目相关处理类
        - Render.php 页面前端渲染处理中间件， 实现统一的前端js值传递
    - (新增) assets/js/Cythina.js 与项目相关的公共处理前端库
    - 系统的版本信息配置到 **config/version.php** 便于系统读取
    - 首页
        - 显示回台返回的系统版本信息

### Cythina-0.0.1@20171118
- 服务器端搭建
    - 基于对 nginx/Apache 等服务器在云上的二级域名的实现，决定在原来的weui的基础上。重构应用； 两者将代表传统的web构建方式以及现代web构建方式的对比
    - 本地搭建 nginx 实验性项目
    - 项目采用最新的php技术 php7.1.x
        - 利用 composer 搭建基于 laravel 5.5.21 的项目 Cythina
        - $ composer create-project --prefer-dist laravel/laravel Cythina
        - $ composer install
    - 创建主键面 /Home 控制器
        - 公共头部设置
- 前端搭建
    - 利用** $ php artisan preset react** 搭建 laravel 的 react基本脚手架
    - 下载 ant-design 2.0.3
        - 使用 npm 管理包 **npm install**
    - 搭建调试， 根据应用的相关错误安装额外的创建
- 基本上实现应用的入门， 基本全部搭建好了系统。    