<?php
/**
 * Auther: Joshua Conero
 * Date: 2018/9/9 0009 14:53
 * Email: brximl@163.com
 * Name: 用户中心
 */

namespace App\Http\Controllers;


use App\Cythina\Cyth;

class User extends Controller
{
    function index(){
        if($this->checkUserNoLogin()){
            return redirect('/');
        }
        $userInfo = Cyth::getUserInfo();
        $render = $this->getRender();
        $render->JsVar('account', $userInfo['account']);
        $render->JsVar('name', $userInfo['name']);

        $render->title($userInfo['account']. '-欢迎您登录系统');

        return $this->page([
            'js'    => 'user/index.js',
            'data'  => $render->toArray()
        ]);
    }
}