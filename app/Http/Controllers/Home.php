<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/11/18 0018 16:18
 * Email: brximl@163.com
 * Name:
 */

namespace App\Http\Controllers;



use Illuminate\Support\Facades\Config;

class Home extends Controller
{
    // 首页
    public function Index(){
        $render = $this->getRender();
        $token = session()->get('token');
        $render->JsVar('date', date('Ymd'))
            ->jsVar('token', $token)
        ;
        return $this->page([
            'js'    => 'home/main.js',
            'data'  => $render->toArray()
        ]);
        //return view('home', $render->toArray());
    }
}