<?php
/**
 * Auther: Joshua Conero
 * Date: 2018/5/8 0008 23:14
 * Email: brximl@163.com
 * Name:
 */

namespace App\Http\Controllers;


class Member extends Controller
{
    /**
     * 系统首页，包括登录和注册
     */
    public function index(){
        $token = session('token');
        $render = $this->getRender();
        $render->JsVar('token', $token)
        ;
        return $this->page([
            'js'    => 'member/index.js',
            'data'  => $render->toArray()
        ]);
    }

    /**
     * 注销登录
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function loginOut(){
        session()->flush();
        return redirect('/');
    }
}