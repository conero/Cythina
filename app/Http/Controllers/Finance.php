<?php
/**
 * Auther: Joshua Conero
 * Date: 2018/9/9 0009 14:52
 * Email: brximl@163.com
 * Name: 财务中心
 */

namespace App\Http\Controllers;


class Finance  extends Controller
{
    /**
     * 财务中心
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function index(){
        if($this->checkUserNoLogin()){
            return redirect('/');
        }
        return $this->page([
            'js' => 'finance/index.js'
        ]);
    }
}