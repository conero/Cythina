<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Cythina\Render;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @return Render
     */
    protected function getRender(){
        return new Render();
    }

    /**
     * @return bool
     */
    protected function checkUserNoLogin(){
        $unlogin = !session()->get('token');
        return $unlogin;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function page($data=array()){
        $data['_page'] = $data['_page'] ?? [];
        if($js = ($data['js'] ?? false)){
            $data['_page']['js'] = asset('/js/'. $js);
            unset($data['js']);
        }
        if($title = ($data['title'] ?? false)){
            $data['_page']['title'] = $title;
            unset($data['title']);
        }
        if($data['data'] ?? false){
            $tData = $data['data'];
            unset($data['data']);
            $data = array_merge($data, $tData);
       }
        return view('page', $data);
    }
}
