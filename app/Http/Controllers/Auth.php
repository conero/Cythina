<?php
/**
 * Auther: Joshua Conero
 * Date: 2018/9/6 0006 23:01
 * Email: brximl@163.com
 * Name:
 */

namespace App\Http\Controllers;


use App\Cythina\Cyth;
use pigeons\nest\Sender;

class Auth extends Controller
{
    /**
     * 用户认证登录
     */
    function login(){
        // 已经登录时禁止
        if(session('token')){
            Cyth::success();
        }
        $pigeon = new Sender(Cyth::envConf('pigeon'));
        $post = $_POST;
        $post['project'] = env('APP_NAME');
        $post['access_token'] = $pigeon->accessToken;
        $result = $pigeon->post('aurora/user/login', $post);
        if($result->isOk){
            $raw = $result->raw;
            $token = $raw['token'] ?? null;
            $openid = $raw['openid'] ?? null;
            if($token && $openid){
                $saveData = [
                    'token' => $token,
                    'openid'=> $openid
                ];
                $info = $pigeon->get('aurora/user/info', ['token'=>$token]);
                $saveData['userInfo'] = $info->data;
                session($saveData);
                return Cyth::success($token);
            }
            return Cyth::error('接口请求成功，但是登录失败！');
        }
        else{
            return Cyth::error($result->msg);
        }
        return Cyth::error('请求参数无效');
    }
}