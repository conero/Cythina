<?php
/**
 * Auther: Joshua Conero
 * Date: 2018/9/9 0009 15:50
 * Email: brximl@163.com
 * Name:
 */

namespace App\Http\Controllers\finance;


use App\Cythina\Cyth;
use App\Http\Controllers\Controller;

class Bill extends Controller
{
    function index(){
        if($this->checkUserNoLogin()){
            return redirect('/');
        }
        $render = $this->getRender();
        $render->JsVar('user', Cyth::getUserInfo('account'));

        return $this->page([
            'js'    => 'finance/bill.js',
           'data'   => $render->toArray()
        ]);
    }
}