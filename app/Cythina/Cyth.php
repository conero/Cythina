<?php
/**
 * Auther: Joshua Conero
 * Date: 2018/9/6 0006 23:09
 * Email: brximl@163.com
 * Name:
 */

namespace App\Cythina;


class Cyth
{
    /**
     * 错误输出
     * @param $msg
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    static function error($msg, $data=null){
        $json = [
            'error' => 1,
            'msg'   => $msg
        ];
        if($data){
            $json['data'] = $data;
        }
        return response()->json($json);
    }
    /**
     * 响应正确
     * @param null|string $msg
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    static function success($msg=null, $data=null){
        if(is_array($msg) && !$data){
            $data = $msg;
            $msg = null;
        }
        $json = [
            'error' => 0,
            'msg'   => $msg ?? ''
        ];
        if($data){
            $json['data'] = $data;
        }
        return response()->json($json);
    }

    /**
     * @param $key
     * @return \Illuminate\Config\Repository|mixed
     */
    static function envConf($key){
        $key .= '.'. env('APP_ENV');
        return config($key);
    }

    /**
     * 获取用户信息
     * @param null $key
     * @return array|null
     */
    static function getUserInfo($key=null){
        $data = session()->get('userInfo');
        $value = $data;
        if($key){
            $value = $data[$key] ?? null;
        }
        return $value;
    }
}